package main

import (
	"fmt"
	"log"

	"gitlab.com/marceloeduardo244/cupcakeworld/bootstrap"
	"gitlab.com/marceloeduardo244/cupcakeworld/config"
)

func main() {
	app := bootstrap.NewApplication()
	addr := fmt.Sprintf("%s:%s", config.GetEnv("APP_HOST", "localhost"), config.GetEnv("APP_PORT", "4000"))

	if config.GetEnv("DEV_MODE", "true") == "true" {
		log.Fatal(app.Listen(addr))
		return
	}

	certFile := config.GetEnv("PATH_CERTFILE", "./certificado.crt")
	keyFile := config.GetEnv("PATH_KEYFILE", "./chave_privada.pem")
	log.Printf("Running in production mode on https://%s", addr)
	log.Fatal(app.ListenTLS(addr, certFile, keyFile))
}
