package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/marceloeduardo244/cupcakeworld/services"
	"gitlab.com/marceloeduardo244/cupcakeworld/views"
)

type DashboardController interface {
	RenderDashboard(ctx *fiber.Ctx) error
}

type dashboardController struct {
	dashboardService services.DashboardService
}

func NewDashboardController(s services.DashboardService) DashboardController {
	return &dashboardController{
		dashboardService: s,
	}
}

func (c *dashboardController) RenderDashboard(ctx *fiber.Ctx) error {
	data := c.dashboardService.GetInfo(30)
	return views.Render(ctx, "dashboard/dashboard", data, "", baseLayout)
}
