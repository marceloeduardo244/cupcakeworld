## Objetivo do projeto
- Este projeto é de caráter estudantil e tem como objetivo criar uma loja que venda deliciosos cupcakes.
- Com niveis de acesso controlados e painel administrativo.


## Documentações
- [Diagrama de casos de uso](https://gitlab.com/marceloeduardo244/cupcakeworld/-/wikis/Diagrama-de-casos-de-uso)
- [Diagrama de classes](https://gitlab.com/marceloeduardo244/cupcakeworld/-/wikis/Diagrama-de-classes)
- [Wireframes](https://gitlab.com/marceloeduardo244/cupcakeworld/-/wikis/Wireframes)
- [Histórias de Usuário](https://gitlab.com/marceloeduardo244/cupcakeworld/-/issues)
- [Projeto formal](https://gitlab.com/marceloeduardo244/cupcakeworld/-/blob/develop/docs/PIT%20-%2001-2024%20-%20Marcelo%20Oliveira.pdf?ref_type=heads)

## Imagens do sistema
![Tela administrativa](https://gitlab.com/marceloeduardo244/cupcakeworld/-/raw/main/docs/admin-page-final.png?ref_type=heads)
![Tela de login](https://gitlab.com/marceloeduardo244/cupcakeworld/-/raw/main/docs/login-page-final.png?ref_type=heads)
![Tela de compras](https://gitlab.com/marceloeduardo244/cupcakeworld/-/raw/main/docs/main-page-final.png?ref_type=heads)

## Videos utilizando o sistema
- Comprando um cupcake: [video](https://gitlab.com/marceloeduardo244/cupcakeworld/-/blob/develop/docs/fluxo-comprando-um-cupcake.webm?ref_type=heads)
- Acessando o painel administrativo: [video](https://gitlab.com/marceloeduardo244/cupcakeworld/-/blob/develop/docs/exibindo-painel-admin.webm?ref_type=heads)

## Deploy da aplicação
- [Informações de deploy](https://gitlab.com/marceloeduardo244/cupcakeworld/-/wikis/Deploy-do-projeto-em-Docker)