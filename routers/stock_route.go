package routers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/marceloeduardo244/cupcakeworld/controllers"
	"gitlab.com/marceloeduardo244/cupcakeworld/database"
	"gitlab.com/marceloeduardo244/cupcakeworld/middlewares"
	"gitlab.com/marceloeduardo244/cupcakeworld/repositories"
	"gitlab.com/marceloeduardo244/cupcakeworld/services"
)

type StockRouter struct {
	stockController controllers.StockController
}

func NewStockRouter() *StockRouter {
	// Initialize repositories
	stockRepository := repositories.NewStockRepository(database.DB)

	// Initialize services with repositories
	stockService := services.NewStockService(stockRepository)

	// Initialize controllers with services
	stockController := controllers.NewStockController(stockService)

	return &StockRouter{
		stockController: stockController,
	}
}

func (r *StockRouter) InstallRouters(app *fiber.App) {
	stock := app.Group("/stock").Use(middlewares.LoginAndStaffRequired())

	stock.Get("/create", r.stockController.RenderCreate)
	stock.Post("/create", r.stockController.Create)
	stock.Get("/", r.stockController.RenderStocks)
	stock.Get("/:id", r.stockController.RenderStock)
}
