package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/marceloeduardo244/cupcakeworld/config"
	"gitlab.com/marceloeduardo244/cupcakeworld/models"
	"gitlab.com/marceloeduardo244/cupcakeworld/services"
	"gitlab.com/marceloeduardo244/cupcakeworld/session"
	"gitlab.com/marceloeduardo244/cupcakeworld/views"
)

type AuthController interface {
	Register(ctx *fiber.Ctx) error
	Login(ctx *fiber.Ctx) error
	Logout(ctx *fiber.Ctx) error
	RenderLogin(ctx *fiber.Ctx) error
	RenderRegister(ctx *fiber.Ctx) error
}

type authController struct {
	authService services.AuthService
}

func NewAuthController(authService services.AuthService) AuthController {
	return &authController{
		authService: authService,
	}
}

func (c *authController) Register(ctx *fiber.Ctx) error {
	user := new(models.User)
	if err := ctx.BodyParser(user); err != nil {
		return views.Render(ctx, "auth/register", nil, "Dados da conta inválidos: "+err.Error())
	}

	profile := &models.Profile{
		FirstName: ctx.FormValue("firstname"),
		LastName:  ctx.FormValue("lastname"),
		User:      *user,
	}

	err := c.authService.Register(profile)
	if err != nil {
		return views.Render(ctx, "auth/register", nil, "Falha ao criar usuário: "+err.Error())
	}

	return ctx.Redirect("/auth/login")
}

func (c *authController) Login(ctx *fiber.Ctx) error {
	email := ctx.FormValue("email")
	password := ctx.FormValue("password")

	err := c.authService.Authenticate(ctx, email, password)
	if err != nil {
		return views.Render(ctx, "auth/login", nil, "Credenciais inválidas ou usuário inativo.")
	}

	return ctx.Redirect(config.GetEnv("REDIRECT_AFTER_LOGIN", "/"))
}

func (c *authController) Logout(ctx *fiber.Ctx) error {
	sess, err := session.Store.Get(ctx)
	if err != nil {
		return err
	}

	err = sess.Destroy()
	if err != nil {
		return err
	}

	return ctx.Redirect(config.GetEnv("REDIRECT_AFTER_LOGOUT", "/"))
}

func (c *authController) RenderLogin(ctx *fiber.Ctx) error {
	return views.Render(ctx, "auth/login", nil, "")
}

func (c *authController) RenderRegister(ctx *fiber.Ctx) error {
	return views.Render(ctx, "auth/register", nil, "")
}
