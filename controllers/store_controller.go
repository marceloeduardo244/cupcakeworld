package controllers

import (
	"github.com/gofiber/fiber/v2"
	"gitlab.com/marceloeduardo244/cupcakeworld/models"
	"gitlab.com/marceloeduardo244/cupcakeworld/services"
	"gitlab.com/marceloeduardo244/cupcakeworld/views"
)

type StoreController interface {
	RenderStore(ctx *fiber.Ctx) error
}

type storeController struct {
	productService services.ProductService
}

func NewStoreController(productService services.ProductService) StoreController {
	return &storeController{
		productService: productService,
	}
}

func (c *storeController) RenderStore(ctx *fiber.Ctx) error {
	query := ctx.Query("q", "")
	page := ctx.QueryInt("page")
	limit := ctx.QueryInt("limit")

	if limit == 0 {
		limit = 4
	}

	filter := models.NewProductFilter(query, page, limit)
	products := c.productService.FindActiveWithStock(filter)

	data := fiber.Map{
		"Products": products,
		"Filter":   filter,
	}

	return views.Render(ctx, "store/store", data, "", storeLayout)
}
