# Use a imagem base do Golang
FROM golang:1.22

# Defina o diretório de trabalho dentro do contêiner
WORKDIR /go/src/app

# Copie tudo
COPY . .

# Baixe as dependências
RUN go mod download

# Construa o aplicativo
RUN go build -o app

# Expor a porta que a aplicação vai rodar
EXPOSE 4000

# Comando para rodar a aplicação
CMD ["./app"]
